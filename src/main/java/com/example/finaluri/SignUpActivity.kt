package com.example.finaluri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        registerbtn.setOnClickListener(){ register() }
    }

    private fun register() {
        if(check()){
            firebase_auth(Emailtext.text.toString() , Passwordtext.text.toString())
        }
    }

    private fun check() : Boolean{
        if(!Patterns.EMAIL_ADDRESS.matcher(Emailtext.text.toString()).matches()) {
            Emailtext.error = "Email format is not acceptable"
            return false
        }
        if(TextUtils.isEmpty(Passwordtext.text.toString()) && Passwordtext.text.toString() != Repeattext.text.toString()){
            Passwordtext.error = "Passwords don't match"
            Repeattext.error = "Passwords don't match"
            return false
        }
        return true
    }

    private fun firebase_auth(username: String , password: String){
        pogbar.visibility = View.VISIBLE
        auth = Firebase.auth
        auth.createUserWithEmailAndPassword(username, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    success(user)
                } else {
                    failure(task)
                }

            }
    }

    fun success(user: FirebaseUser?){
        pogbar.visibility = View.GONE
        Log.d("Firebase", "createUserWithEmail:success")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun failure(task: Task<AuthResult>){
        pogbar.visibility = View.GONE
        Log.w("Firebase", "createUserWithEmail:failure", task.exception)
        Toast.makeText(baseContext, "Authentication failed.",
            Toast.LENGTH_SHORT).show()
    }
}

