package com.example.finaluri

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*


class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        Signinbtn.setOnClickListener(){login()

        }
    }

    private fun login(){
        if(check()){
            firebase_sign(Emailsign.text.toString(), passwordsign.text.toString())
        }
    }

    private fun check(): Boolean{
        if(!Patterns.EMAIL_ADDRESS.matcher(Emailsign.text.toString()).matches()) {
            Emailsign.error = "Email format is not acceptable"
            return false
        }
        if(TextUtils.isEmpty(passwordsign.text.toString())){
            return false
        }
        return true
    }

    private fun firebase_sign(email: String, password: String){
        pogbarsign.visibility = View.VISIBLE
        auth = Firebase.auth
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    successin(user , email)
                } else {
                    failurein(task)
                }
            }
    }

    fun successin(User: FirebaseUser? , email: String){
        pogbarsign.visibility = View.GONE
        Log.d("firebase", "signInWithEmail:success")
        save_email(email)
        val intent = Intent(this, ListActivity::class.java)
        startActivity(intent)
    }

    fun failurein(task: Task<AuthResult>){
        pogbarsign.visibility = View.GONE
        Log.w("firebase", "signInWithEmail:failure", task.exception)
        Toast.makeText(baseContext, "Authentication failed.",
                Toast.LENGTH_SHORT).show()
    }

    fun save_email(email: String){
        val editor = getSharedPreferences("apppref", MODE_PRIVATE).edit()
        editor.putString("email", email)
        editor.apply()
    }
}