package com.example.finaluri

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_adding.*
import java.util.*


class AddingActivity : AppCompatActivity() {

    private fun setAlarm(context: Context) {
        var alarmMgr: AlarmManager? = null
        lateinit var alarmIntent: PendingIntent

        alarmMgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(context, AlarmReceiver::class.java).let { intent ->
            PendingIntent.getBroadcast(context, 0, intent, 0)
        }

        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, datePicker1.hour)
            set(Calendar.MINUTE, datePicker1.minute)
            set(Calendar.SECOND, 0)
        }

        alarmMgr?.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                alarmIntent
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding)

        val prefs = getSharedPreferences("apppref", MODE_PRIVATE)
        val emailpref = prefs.getString("email", "No name defined")

        val username = emailpref?.split("@")?.toTypedArray()

        var database: DatabaseReference = Firebase.database.getReferenceFromUrl("https://finaluri-5c1eb-default-rtdb.firebaseio.com/")

        fun writeNewMedicine(email: String) {
            val med = Medicine(Medicine_name.text.toString(), quantity.text.toString().toInt(), datePicker1.hour, datePicker1.minute)
            val child = Medicine_name.text.toString() + datePicker1.hour.toString() + datePicker1.minute.toString()
            database.child(email).child(child).setValue(med)

        }

        AddingButton.setOnClickListener() {
            username?.elementAt(0)?.let {  writeNewMedicine(it) }

            setAlarm(applicationContext)

            val intent = Intent(this, ListActivity::class.java)
            startActivity(intent)
        }



    }
}