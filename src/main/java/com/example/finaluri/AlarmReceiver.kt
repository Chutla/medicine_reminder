package com.example.finaluri

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat

private val NOTIFICATION_ID = 0
private val REQUEST_CODE = 0
private val FLAGS = 0

class AlarmReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
          Toast.makeText(context, "Time to take pills", Toast.LENGTH_SHORT).show()

        fun NotificationManager.sendNotification(messageBody: String, applicationContext: Context) {
            val contentIntent = Intent(applicationContext, MainActivity::class.java)
            val contentPendingIntent = PendingIntent.getActivity(
                    applicationContext,
                    NOTIFICATION_ID,
                    contentIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )

            val builder = NotificationCompat.Builder(
                    applicationContext,
                    applicationContext.getString(R.string.notification_text)
            )



        // TODO: Step 1.9 add call to sendNotification
        val notificationManager = ContextCompat.getSystemService(
                context,
                NotificationManager::class.java
        ) as NotificationManager

        notificationManager.
        sendNotification(
                context.getText(R.string.notification_text) as String,
                context
        )

            notify(NOTIFICATION_ID, builder.build())

    }

}
}