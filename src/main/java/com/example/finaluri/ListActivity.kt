package com.example.finaluri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {
    lateinit var medicines: ArrayList<Medicine>
    lateinit var rvMedicine: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        populateMedicines()

        rvMedicine = findViewById<View>(R.id.list) as RecyclerView
//        val adapter = Medicineadapter(medicines)
//        rvMedicine.adapter = adapter
//        rvMedicine.layoutManager = LinearLayoutManager(this)

        AddButton.setOnClickListener(){
            val intent = Intent(this, AddingActivity::class.java)
            startActivity(intent)
        }

    }

    fun populateMedicines() {
          medicines = ArrayList()
//        medicines.add(Medicine("a", 1, 1, 2))
//        medicines.add(Medicine("b", 2, 2, 3))
//        medicines.add(Medicine("bv", 3, 3, 12))
        var database: DatabaseReference = Firebase.database.getReferenceFromUrl("https://finaluri-5c1eb-default-rtdb.firebaseio.com/")
        retrieve(database)
    }

    fun retrieve(medicineReference: DatabaseReference) {
        val medicineListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val prefs = getSharedPreferences("apppref", MODE_PRIVATE)
                val emailpref = prefs.getString("email", "No name defined")
                val username = emailpref?.split("@")?.toTypedArray()?.get(0)

                medicines.clear()

                dataSnapshot.child(username!!).children.forEach { child ->
                    val medicine: Medicine? = child.getValue<Medicine>()
                    medicine?.let { medicines.add(medicine) }
                }

                val adapter = Medicineadapter(medicines)
                rvMedicine.adapter = adapter
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(applicationContext, "Error retrieving data", Toast.LENGTH_LONG).show()
                //Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        medicineReference.addValueEventListener(medicineListener)
    }
}