package com.example.finaluri

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Medicine(val name: String? = null, val quantity: Int? = null, val hour: Int? = null , val minute: Int? = null) {

}