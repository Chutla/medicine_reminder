package com.example.finaluri

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Medicineadapter (private val mMedicines: List<Medicine>): RecyclerView.Adapter<Medicineadapter.ViewHolder>() {

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val nameTextView = itemView.findViewById<TextView>(R.id.nameinlist)
        val quantityTextView = itemView.findViewById<TextView>(R.id.quantityinlist)
        val timeTextView = itemView.findViewById<TextView>(R.id.timeinlist)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Medicineadapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val contactView = inflater.inflate(R.layout.items, parent, false)
        return ViewHolder(contactView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val medicine: Medicine = mMedicines.get(position)
        holder.nameTextView.setText(medicine.name)
        holder.quantityTextView.setText(medicine.quantity.toString())
        holder.timeTextView.setText("%02d".format(medicine.hour) + ":" + "%02d".format(medicine.minute))
    }

    override fun getItemCount(): Int {
        return mMedicines.size
    }

}